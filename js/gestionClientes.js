var clientesObtenidos;

function getClientes(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function(){
    // el 4 significa que el reuqest termino de cargar
    if(this.readyState == 4 && this.status == 200){
      //console.table(JSON.parse(request.responseText).value);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET",url,true);
  request.send();
}

function procesarClientes(){
  var JSONClientes = JSON.parse(clientesObtenidos);
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  var divTabla = document.getElementById("divTabla");
  var tabla=document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  //alert(JSONClientes.value[0].ProductName);
  for (var i = 0; i < JSONClientes.value.length; i++) {
    //console.log(JSONClientes.value[i].ProductName);
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].ContactName;

    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = JSONClientes.value[i].City;

    var columnaStock = document.createElement("td");
    var imgBandera=document.createElement("img");
    imgBandera.classList.add("flag");
    if(JSONClientes.value[i].Country == "UK")
      imgBandera.src=rutaBandera+"United-Kingdom.png";
    else
      imgBandera.src=rutaBandera+JSONClientes.value[i].Country+".png";
    columnaStock.appendChild(imgBandera);

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);

  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
